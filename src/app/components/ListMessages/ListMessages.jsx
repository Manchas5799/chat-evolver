import './ListMessages.css';
import { useEffect, useRef, useState } from 'react';
import { format } from 'date-fns';
import { endpoints } from '../../services/endpoints';
import SendIcon from '../../icons/SendIcon';
import MessageLoading from '../MessageLoading/MessageLoading';
import ChatMessagesService from '../../services/ChatMessages.service';

const ListMessages = () => {
  const [chatMessages, setChatMessages] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [sessionId, setSessionId] = useState('');
  const chatMessagesContent = useRef(null);
  const chatMessagesService = new ChatMessagesService();

  useEffect(() => {
    const messagesContainer = chatMessagesContent.current;
    messagesContainer.scrollTop = messagesContainer.scrollHeight;
  }, [chatMessages]);

  const triggerSend = event => {
    if (event.keyCode === 13) {
      sendMessage();
    }
  };

  const sendMessage = async () => {
    const message = document.querySelector('.message').value;
    if (message === '') return;
    document.querySelector('.message').value = '';
    const newMessage = {
      message,
      from: 'user',
      date: new Date(),
    };
    addChatMessage(newMessage);
    setIsLoading(true);
    const { answer, sessionId: sesionId } = await chatMessagesService.post({
      headers: {
        'Session-Id': sessionId,
      },
      body: { text: message },
    });
    setIsLoading(false);
    setSessionId(sesionId);
    parseChatMessages(answer);
  };
  const addChatMessage = message => {
    console.log('error');
    setChatMessages(oldArray => [...oldArray, message]);
  };

  const parseChatMessages = messages => {
    if (!messages) return;
    const answers = messages.split('<code>');
    for (let ans of answers) {
      if (ans.includes('</code>')) {
        const answerClear = ans.split('</code>')[0];
        const aiMessage = {
          message: answerClear,
          from: 'ai',
          date: new Date(),
        };
        addChatMessage(aiMessage);
      }
    }
  };

  return (
    <div className="chat-container">
      <div className="messages-container" ref={chatMessagesContent}>
        {chatMessages.map((message, index) => (
          <div key={index} className={message.from + ' message-history'}>
            <p>{message.message}</p>
            <span className="date">{format(message.date, 'Pp')}</span>
          </div>
        ))}
        {isLoading && <MessageLoading />}
      </div>
      <div className="field">
        <input
          className="message"
          type="text"
          name="message"
          onKeyUp={triggerSend}
        />
        <button className="send" type="button" onClick={sendMessage}>
          <SendIcon />
        </button>
      </div>
    </div>
  );
};

export default ListMessages;
