import { useEffect, useState } from 'react';
import TemplatesService from '../../services/Templates.service.js';
const ListTemplates = () => {
  const [templates, setTemplates] = useState([]);
  useEffect(() => {
    initTemplates();
  }, []);
  const initTemplates = async () => {
    const templatesService = new TemplatesService();
    const { data: templatesFromBack } = await templatesService.get();
    setTemplates(templatesFromBack);
  };
  return (
    <div className="list-templates-container">
      {templates.length > 0 ? (
        templates.map(template => (
          <div key={template.name} className="template">
            <h3>{template.name}</h3>
          </div>
        ))
      ) : (
        <p>No hay templates</p>
      )}
    </div>
  );
};

export default ListTemplates;
