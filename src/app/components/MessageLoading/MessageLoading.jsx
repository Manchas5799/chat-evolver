import './MessageLoading.css';
const MessageLoading = () => {
  return (
    <section className="message-loading-container">
      <div className="message-loading">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </section>
  );
};

export default MessageLoading;
