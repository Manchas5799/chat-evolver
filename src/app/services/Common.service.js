'use strict';
import env from '../../../env.json';
export class CommonService {
  async get(params = { method: 'GET', body: {}, headers: {} }, objectIsFail = {}) {
    return await this.execFetch({ method: 'GET', ...params }, objectIsFail);
  }

  async post(params = { method: 'POST', body: {}, headers: {} }, objectIsFail = {}) {
    return await this.execFetch({ method: 'POST', ...params }, objectIsFail);
  }

  createRequest(params = { method: 'GET', body: {}, headers: {} }) {

    return {
      method: params.method,
      headers: {
        'Content-Type': 'application/json',
        ...params.headers,
      },
      body: Object.keys(params.body).length > 0 ? JSON.stringify(params.body) : undefined,
    };
  }

  async execFetch(params = {}, objectIsFail = {}) {
    const request = this.createRequest(params);
    try {
      const response = await fetch(
        `${env.api_chat + this.path}`, request);
      const data = await response.json();
      if (!response.ok) {
        return objectIsFail;
      }

      return data;
    } catch (error) {
      console.log(error);
      return objectIsFail;
    }
  }
}
