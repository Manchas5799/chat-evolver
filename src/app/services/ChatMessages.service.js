import { CommonService } from './Common.service';
import { endpoints } from './endpoints';

export default class ChatMessagesService extends CommonService {
  constructor() {
    super();
    this.path = endpoints.chat;
  }
}
