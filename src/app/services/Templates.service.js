import { CommonService } from './Common.service';
import { endpoints } from './endpoints';

export default class TemplatesService extends CommonService {
  constructor() {
    super();
    this.path = endpoints.templates;
  }
}
