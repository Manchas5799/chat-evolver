import './App.css';

import ListTemplates from './app/components/ListTemplates/ListTemplates';
import ListMessages from './app/components/ListMessages/ListMessages';
function App() {
  return (
    <div className="evolver-chat">
      <div className="header">
        <h1>Chat Evolver</h1>
      </div>
      <div className="templates-container">
        <ListTemplates />
      </div>
      <ListMessages />
    </div>
  );
}

export default App;
