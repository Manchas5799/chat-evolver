FROM node:16-alpine3.11 as build-stage

# Create app directory
WORKDIR /app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Copy app source code
COPY . .
COPY env.production.json env.json

RUN npm run build

# Bundle app source
FROM nginx:1.21.1-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
